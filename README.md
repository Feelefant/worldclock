# WorldClock

## Clock Desktop Widget
A simple tkinter program to show local and foreign time.

## Description
This simple app can run in the background on the top most level of available display windows. By adding locales to the dictionary of time zones, more clocks can be shown on screen. The theme is available in dark and light.
All Changes need to be done inside the code base since I haven't added a configuration page yet. Maybe this will follow soon.

**References:**

+ [Clock-Widget](https://github.com/znkldev/Desktop-Clock-Widget/blob/main/Clock-Widget(EN).py)
+ [Clocky](https://github.com/spignelon/Clocky/blob/main/Clocky.py)
+ [Tutorial](https://www.digitalocean.com/community/tutorials/tkinter-working-with-classes)
+ [Timezone Database](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)

## Installation
You need to install [tkinter](https://docs.python.org/3/library/tkinter.html#module-tkinter) and the [Sun-Valley-ttk-theme](https://github.com/rdbende/Sun-Valley-ttk-theme).
If you run it inside a [python virtual environment](https://djangocentral.com/how-to-a-create-virtual-environment-for-python/) the flag `--system-site-packages` needs to be used for generation. tkinter can't be installed inside the *venv*.
