# Referenz No. 1: https://github.com/znkldev/Desktop-Clock-Widget/blob/main/Clock-Widget(EN).py
# Referenz No. 2: https://github.com/spignelon/Clocky/blob/main/Clocky.py
# Class in tkinter: https://www.digitalocean.com/community/tutorials/tkinter-working-with-classes
import zoneinfo as zi
from datetime import datetime, timezone
import tkinter as tk
from tkinter import ttk
import sv_ttk # Installed Sun-Valley-ttk-theme: https://github.com/rdbende/Sun-Valley-ttk-theme

class Windows(tk.Tk):
    """Class for generating generic application GUIs. Functionality for 
    additional windows and switching between them is already implemented."""
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        # Define a set style with color scheme
        sv_ttk.set_theme('dark') # Two themes available: light and dark

        # Adding title to Window
        self.wm_title("World Clock")

        # Creating frame inside the Window and adding it to container
        container = tk.Frame(self)
        # Specifying Frame position inside the Window
        container.grid(column=0, row=0)

        # Getting screen height and width, position object on screen
        self.screen_height = self.winfo_screenheight()
        self.widget_height = self.winfo_reqheight()
        self.total_height = self.screen_height - self.widget_height
        self.y_pos_factor = 1 # Position factor for vertical position
        self.screen_width = self.winfo_screenwidth()
        self.widget_width = self.winfo_reqwidth()
        self.total_width = self.screen_width - self.widget_width
        self.x_pos_factor = 0 # Position factor for horizontal position
        self.x_pos = round(self.total_width * self.x_pos_factor)
        self.y_pos = round(self.total_height * self.y_pos_factor)
        self.geometry(f"+{self.x_pos}+{self.y_pos}") # Format String: 'window_widthxwindow_height+x_position+y_position'

        # Removing window frame, set widget always on top, making it not resizeable
        self.overrideredirect(True)
        self.wm_attributes('-topmost',1)
#        self.wm_attributes('-alpha',0) # Transparency is not recommended, since the behaviour is different between platforms
        self.resizable(0, 0)  # Making the window size fixed

        # Create a Dictionary of frames
        self.frames = {}
        # Add the components to the dictionary
        for F in (MainPage,): # If more Pages are desired, they can be added here
            frame = F(container, self) # Instantiates a class with container as parent and a Windows instance as controller

            # The windows class acts as the root window for the frames
            self.frames[F] = frame # Adds the class name as key and the class ID as value to the dictionary
            frame.grid(row=0, column=0, sticky="nsew")

        # Using a method to switch frames, only if more than one page is inside the dictionary of frames
        self.show_frame(MainPage)

        # End Programm when Window clicked
        self.bind("<B1-Motion>", self.kill_window)
        self.bind("<ButtonRelease-1>", self.kill_window)

    def kill_window(self, event):
        self.destroy()

    def show_frame(self, cont): # Only necessary if more then one Page is available, for usage in button a Lambda function is required
        frame = self.frames[cont]
        # Raises the current frame to the top
        frame.tkraise()

class World_Clock:
    """Clock class. The root widget (root) as well as the Timezone (timez)
    need to be given.
    Find database for Timezones at: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones"""
    def __init__(self, root, timez):
        self.timez = timez
        self.root = root

    def clock(self,tzone):
        current_date_time = datetime.now()
        time_string = current_date_time.astimezone(tzone).strftime("%H:%M:%S")
        self.clock_label.config(text=time_string, font=("Montserrat", 12, "bold"))
        self.root.after(1000, self.clock, tzone) # .after method is needed to continuously update the clock

    def clock_window(self):
        self.clock_label = ttk.Label(master=self.root, text="")
        self.clock_label.grid(column=0, row=0)
        self.clock(self.timez)

class MainPage(tk.Frame):
    """Simple Page design. Can be used as template for additional Pages. Functionality is
    implemented on each individual page."""
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)

        self.config(bd=2, relief='solid')

        # The following dictionary delivers the country name and the locale to the generating loop.
        locales = {
                    'GERMANY':zi.ZoneInfo("Europe/Berlin"),
                    'CHENGDU':zi.ZoneInfo("Asia/Shanghai"),
                    #'LOS ANGELES':zi.ZoneInfo("America/Los_Angeles"),
                    #'Port of Spain':zi.ZoneInfo("America/Port_of_Spain"),
                    #'ACCRA':zi.ZoneInfo("Africa/Accra"),
                }

        for key, locale in locales.items():
            widget_label = ttk.Label(self, text=f"{key.title()}:", font=("Montserrat", 10,))
            widget_label.grid(column=0, row=list(locales).index(key), padx=10, pady=5)

            widget_frame = ttk.Frame(self)
            widget_frame.grid(column=1, row=list(locales).index(key), padx=10, pady=5)

            clock = World_Clock(widget_frame, locale)
            clock.clock_window()

if __name__ == "__main__":
    testObj = Windows()
    testObj.mainloop()
